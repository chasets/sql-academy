************************************************
Welcome to Aginity SQL Academy's documentation!
************************************************

.. Aginity SQL Academy documentation master file, created by
   sphinx-quickstart on Thu May 23 12:00:09 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
   :caption: SQL Academy
   :maxdepth: 3


   Academy for Data Analysts <docs/analysts/index>
   Academy for Data Engineers <docs/engineer/index>
