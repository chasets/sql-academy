
************************************************
SQL Academy for Data Engineers
************************************************

.. Aginity SQL Academy documentation master file, created by
   sphinx-quickstart on Thu May 23 12:00:09 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
   :caption: SQL Academy for Data Engineers
   :maxdepth: 2


   redshift_utilities
   generate_data_profile_sql
   redshift_cat_picture
