
************************************************
SQL Academy for Data Analysts
************************************************

.. Aginity SQL Academy documentation master file, created by
   sphinx-quickstart on Thu May 23 12:00:09 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. toctree::
   :caption: SQL Academy for Data Analysts
   :maxdepth: 2


   irr_internal_rate_of_return
   RFM
   regression
